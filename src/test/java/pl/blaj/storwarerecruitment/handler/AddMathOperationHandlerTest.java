package pl.blaj.storwarerecruitment.handler;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import pl.blaj.storwarerecruitment.model.MathOperation.Type;

public class AddMathOperationHandlerTest {

  private AddMathOperationHandler addMathOperationHandler;

  @BeforeEach
  public void setUp() {
    addMathOperationHandler = new AddMathOperationHandler();
  }

  @Test
  public void whenNotSupported_isSupported_shouldReturnFalse() {
    // given
    var nonSupportedType = Type.MULTIPLY;

    // when
    var isSupported = addMathOperationHandler.isSupported(nonSupportedType);

    // then
    assertThat(isSupported).isFalse();
  }

  @Test
  public void whenSupported_isSupported_shouldReturnTrue() {
    // given
    var supportedType = Type.ADD;

    // when
    var isSupported = addMathOperationHandler.isSupported(supportedType);

    // then
    assertThat(isSupported).isTrue();
  }

  @Test
  public void whenNumber5AndBase10_calculate_shouldReturn15() {
    // given
    var number = 5;
    var base = 10;

    // when
    var result = addMathOperationHandler.calculate(number, base);

    // then
    assertThat(result).isEqualTo(15);
  }
}
