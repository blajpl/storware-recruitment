package pl.blaj.storwarerecruitment.service;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import pl.blaj.storwarerecruitment.model.MathOperation;
import pl.blaj.storwarerecruitment.model.MathOperation.Type;

public class MathServiceTest {

  private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();

  private MathService mathService;

  @BeforeEach
  public void setUp() {
    System.setOut(new PrintStream(outContent));

    mathService = new MathService();
  }

  @Test
  public void whenApply10Add2Multiply3_doMathOperations_shouldPrint36() {
    // given
    var mathOperations =
        Arrays.asList(
            mathOperation(2, Type.ADD),
            mathOperation(3, Type.MULTIPLY),
            mathOperation(10, Type.APPLY));

    // when
    mathService.doMathOperations(mathOperations);

    // then
    assertThat(outContent.toString().trim()).isEqualTo("Result: 36");
  }

  @Test
  public void whenApply5Multiply5Add10_doMathOperations_shouldPrint35() {
    var mathOperations =
        Arrays.asList(
            mathOperation(5, Type.MULTIPLY),
            mathOperation(10, Type.ADD),
            mathOperation(5, Type.APPLY));

    // when
    mathService.doMathOperations(mathOperations);

    // then
    assertThat(outContent.toString().trim()).isEqualTo("Result: 35");
  }

  private MathOperation mathOperation(Integer number, Type type) {
    return new MathOperation().setNumber(number).setType(type);
  }
}
