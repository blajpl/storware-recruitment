package pl.blaj.storwarerecruitment.validator;

import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import pl.blaj.storwarerecruitment.model.MathOperation;
import pl.blaj.storwarerecruitment.model.MathOperation.Type;

public class MathServiceValidatorTest {

  private MathServiceValidator mathServiceValidator;

  @BeforeEach
  public void setUp() {
    mathServiceValidator = new MathServiceValidator();
  }

  @Test
  public void whenTypeNotExists_validate_shouldThrowException() {
    // given
    var mathOperations = List.of(new MathOperation().setType(null).setNumber(1));

    // when
    assertThrows(Exception.class, () -> mathServiceValidator.validate(mathOperations));

    // then
  }

  @Test
  public void whenNumberNotExists_validate_shouldThrowException() {
    // given
    var mathOperations = List.of(new MathOperation().setType(Type.APPLY).setNumber(null));

    // when
    assertThrows(Exception.class, () -> mathServiceValidator.validate(mathOperations));

    // then
  }

  @Test
  public void whenApplyExistsMoreOneTimes_validate_shouldThrowException() {
    // given
    var mathOperations =
        List.of(
            new MathOperation().setType(Type.APPLY).setNumber(1),
            new MathOperation().setType(Type.APPLY).setNumber(2));

    // when
    assertThrows(Exception.class, () -> mathServiceValidator.validate(mathOperations));

    // then
  }
}
