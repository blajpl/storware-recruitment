package pl.blaj.storwarerecruitment.mapper;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import pl.blaj.storwarerecruitment.model.MathOperation;
import pl.blaj.storwarerecruitment.model.MathOperation.Type;

public class MathOperationMapperTest {

  @Test
  public void whenNull_map_shouldReturnNull() {
    // given
    String line = null;

    // when
    var dto = MathOperationMapper.map(line);

    // then
    assertThat(dto).isNull();
  }

  @Test
  public void whenNotNull_map_shouldReturnMathOperation() {
    // given
    var line = "Apply 10";

    // when
    var dto = MathOperationMapper.map(line);

    // then
    assertThat(dto).isNotNull();
    assertThat(dto.getNumber()).isEqualTo(10);
    assertThat(dto.getType()).isEqualTo(Type.APPLY);
  }
}
