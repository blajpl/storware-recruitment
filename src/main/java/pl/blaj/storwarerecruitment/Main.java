package pl.blaj.storwarerecruitment;

import java.net.URL;
import java.util.Optional;
import pl.blaj.storwarerecruitment.service.MathService;
import pl.blaj.storwarerecruitment.util.FileUtil;

public class Main {

  private final MathService mathService;

  public Main() {
    mathService = new MathService();
  }

  public static void main(String[] args) throws Exception {
    var main = new Main();

    var file =
        FileUtil.readFile(
            Optional.ofNullable(Main.class.getClassLoader().getResource("math_operation.txt"))
                .map(URL::getFile)
                .orElseThrow(() -> new Exception("File not exists")));

    var mathOperations = main.mathService.getMathOperations(file);
    main.mathService.doMathOperations(mathOperations);
  }
}
