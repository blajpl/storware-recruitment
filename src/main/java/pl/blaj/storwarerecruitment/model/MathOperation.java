package pl.blaj.storwarerecruitment.model;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class MathOperation {

  private Integer number;

  private Type type;

  public enum Type {
    ADD,
    SUB,
    DIVIDE,
    MULTIPLY,
    APPLY
  }
}
