package pl.blaj.storwarerecruitment.mapper;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import pl.blaj.storwarerecruitment.model.MathOperation;
import pl.blaj.storwarerecruitment.model.MathOperation.Type;

public class MathOperationMapper {

  public static MathOperation map(String line) {
    return Optional.ofNullable(line)
        .map(l -> new MathOperation().setNumber(getNumber(line)).setType(getType(line)))
        .orElse(null);
  }

  private static Integer getNumber(String line) {
    return Optional.ofNullable(getSplitLine(line, 1)).map(Integer::valueOf).orElse(null);
  }

  private static Type getType(String line) {
    var splitLine = getSplitLine(line, 0);

    return Arrays.stream(Type.values())
        .filter(t -> t.name().equalsIgnoreCase(splitLine))
        .findFirst()
        .orElse(null);
  }

  private static String getSplitLine(String line, int index) {
    var splittedLine = List.of(line.split(" "));

    if (splittedLine.size() < 2) {
      return null;
    }

    return splittedLine.get(index);
  }
}
