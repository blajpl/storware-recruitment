package pl.blaj.storwarerecruitment.validator;

import java.util.List;
import pl.blaj.storwarerecruitment.model.MathOperation;
import pl.blaj.storwarerecruitment.model.MathOperation.Type;

public class MathServiceValidator {

  public void validate(List<MathOperation> mathOperations) throws Exception {
    for (var mathOperation : mathOperations) {
      validateTypeExists(mathOperation.getType());
      validateNumberExists(mathOperation.getNumber());
    }

    validateApplyIsOnlyOne(mathOperations);
  }

  private void validateTypeExists(Type type) throws Exception {
    if (type == null) {
      throw new Exception("Unsupported type");
    }
  }

  private void validateNumberExists(Integer number) throws Exception {
    if (number == null) {
      throw new Exception("2nd argument must be a number");
    }
  }

  private void validateApplyIsOnlyOne(List<MathOperation> mathOperations) throws Exception {
    var count = mathOperations.stream().filter(mo -> mo.getType().equals(Type.APPLY)).count();

    if (count > 1) {
      throw new Exception("Apply type must be one in file");
    }
  }
}
