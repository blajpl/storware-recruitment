package pl.blaj.storwarerecruitment.handler;

import pl.blaj.storwarerecruitment.model.MathOperation.Type;

public class SubMathOperationHandler implements MathOperationHandler {

  private static final Type SUPPORTED_TYPE = Type.SUB;

  @Override
  public boolean isSupported(Type type) {
    return type.equals(SUPPORTED_TYPE);
  }

  @Override
  public Integer calculate(Integer number, Integer base) {
    return base - number;
  }
}
