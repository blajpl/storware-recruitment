package pl.blaj.storwarerecruitment.handler;

import pl.blaj.storwarerecruitment.model.MathOperation.Type;

public class AddMathOperationHandler implements MathOperationHandler {

  private static final Type SUPPORTED_TYPE = Type.ADD;

  @Override
  public boolean isSupported(Type type) {
    return type.equals(SUPPORTED_TYPE);
  }

  @Override
  public Integer calculate(Integer number, Integer base) {
    return base + number;
  }
}
