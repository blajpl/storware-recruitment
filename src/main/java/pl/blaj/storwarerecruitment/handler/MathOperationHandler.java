package pl.blaj.storwarerecruitment.handler;

import pl.blaj.storwarerecruitment.model.MathOperation.Type;

public interface MathOperationHandler {

  boolean isSupported(Type type);

  Integer calculate(Integer number, Integer base);

}
