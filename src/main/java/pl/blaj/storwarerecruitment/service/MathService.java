package pl.blaj.storwarerecruitment.service;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Scanner;
import java.util.Set;
import pl.blaj.storwarerecruitment.Main;
import pl.blaj.storwarerecruitment.handler.AddMathOperationHandler;
import pl.blaj.storwarerecruitment.handler.ApplyMathOperationHandler;
import pl.blaj.storwarerecruitment.handler.DivideMathOperationHandler;
import pl.blaj.storwarerecruitment.handler.MathOperationHandler;
import pl.blaj.storwarerecruitment.handler.MultiplyMathOperationHandler;
import pl.blaj.storwarerecruitment.handler.SubMathOperationHandler;
import pl.blaj.storwarerecruitment.mapper.MathOperationMapper;
import pl.blaj.storwarerecruitment.model.MathOperation;
import pl.blaj.storwarerecruitment.model.MathOperation.Type;
import pl.blaj.storwarerecruitment.util.FileUtil;
import pl.blaj.storwarerecruitment.validator.MathServiceValidator;

public class MathService {

  private final Set<MathOperationHandler> mathOperationHandlers = new HashSet<>();

  private final MathServiceValidator mathServiceValidator;

  public MathService() {
    mathOperationHandlers.add(new ApplyMathOperationHandler());
    mathOperationHandlers.add(new AddMathOperationHandler());
    mathOperationHandlers.add(new SubMathOperationHandler());
    mathOperationHandlers.add(new DivideMathOperationHandler());
    mathOperationHandlers.add(new MultiplyMathOperationHandler());

    mathServiceValidator = new MathServiceValidator();
  }

  public List<MathOperation> getMathOperations(File file) throws Exception {
    var mathOperations = new ArrayList<MathOperation>();
    var scanner = new Scanner(file);

    while (scanner.hasNextLine()) {
      mathOperations.add(MathOperationMapper.map(scanner.nextLine()));
    }

    scanner.close();

    return mathOperations;
  }

  public void doMathOperations(List<MathOperation> mathOperations) {
    try {
      mathServiceValidator.validate(mathOperations);
      calculate(mathOperations);
    } catch (Exception ex) {
      System.out.println(ex.getMessage());
    }
  }

  private void calculate(List<MathOperation> mathOperations) {
    var result = 0;

    mathOperations.sort((o1, o2) -> o1.getType().equals(Type.APPLY) ? -1 : 0);

    for (var mathOperation : mathOperations) {
      var mathOperationHandler = fetchMathOperationHandler(mathOperation);

      if (mathOperationHandler == null) {
        continue;
      }

      result = mathOperationHandler.calculate(mathOperation.getNumber(), result);
    }

    System.out.println("Result: " + result);
  }

  private MathOperationHandler fetchMathOperationHandler(MathOperation mathOperation) {
    return mathOperationHandlers.stream()
        .filter(moh -> moh.isSupported(mathOperation.getType()))
        .findFirst()
        .orElse(null);
  }
}
