package pl.blaj.storwarerecruitment.util;

import java.io.File;
import java.util.Optional;

public class FileUtil {

  public static File readFile(String fileName) throws Exception {
    return new File(
        Optional.ofNullable(fileName).orElseThrow(() -> new Exception("File not found")));
  }
}
