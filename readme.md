## Storware zadanie rekrutacyjne ##

### Opis ###
Zadanie zostało zrobione z wykorzystaniem JDK 11 i Maven'a. W projekcie znajduje się kilka przykładowych testów jednostkowych.

### Wymgania ###
* JDK 11
* Maven

### Kompilacja ###
1. Przechodzimy do głownego katalogu z projektem
2. Wpisujemy komendę ``mvn clean install``
3. Skompilowana klasa jest dostępna w folderze ``target``